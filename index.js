const StateContext = require('./src/state_context.js');
const drone = require('./src/drone.js');
const LaunchState = require('./src/states/launch_state.js');
const LookAroundState = require('./src/states/look_around_state.js');
const QRAltitudeState = require('./src/states/qr_altitude_state.js');
const QRLookoutState = require('./src/states/qr_lookout_state.js');
const RingAltitudeState = require('./src/states/ring_altitude_state.js');
const RingLookoutState = require('./src/states/ring_lookout_state.js');
const FlyThroughRingState = require('./src/states/fly_though_ring_state.js');

const stateMapping = {};
stateMapping['launch'] = new LaunchState();
stateMapping['lookaround'] = new LookAroundState();
stateMapping['qraltitude'] = new QRAltitudeState();
stateMapping['qrlookout'] = new QRLookoutState();
stateMapping['ringaltitude'] = new RingAltitudeState();
stateMapping['ringlookout'] = new RingLookoutState();
stateMapping['flythroughring'] = new FlyThroughRingState();
 
const stateContext = new StateContext(drone);
//select entry point
stateContext.setState(stateMapping['launch']);
var promise;

promise = stateContext.handle();
promise.then(onHandled);

function onHandled(nextState) {
    if (nextState === null) {
        console.log('next state is null');
        return;
    }

    console.log('Setting state ' + nextState);
    let state = stateMapping[nextState];

    if (!state) {
        throw new Error('No state with id ' + nextState + ' in stateMapping.');
    }

    stateContext.setState(state);
    promise = stateContext.handle();
    promise.then(onHandled);
}
