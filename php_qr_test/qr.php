<?php
    if($argc < 2) {
        echo "php qr.php <image>";
        return;
    }

    require __DIR__ . "/vendor/autoload.php";

    $qrCode = new QrReader($argv[1]);
    $data = $qrCode->text();

    echo $data;
?>
