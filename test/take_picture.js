const arDrone = require('ar-drone');
const drone = require('./../src/drone.js');
const cv = require('opencv');
const fs = require('fs');

const client = drone.client;
var imgStream = new cv.ImageStream();
client.getPngStream().pipe(imgStream);

const path = __dirname + "/test_images/";

//drone.switchCam();

setInterval(() => {
    writeImage();
}, 500);

function writeImage() {
    if (!drone.image) {
        console.log("Image is null");
        return;
    }

    let fn = `qr_${Date.now()}.png`;
    console.log("Saving image with filename " + path + fn);
    drone.image.save(path + fn)
}