const arDrone = require('ar-drone');
const cv = require('opencv');
const fs = require('fs');
const QrCode = require('qrcode-reader');
const PNG = require('png-js');
const drone = require('./drone.js');

var lowThresh = 0;
var highThresh = 100;
var minArea = 1500;

var RED = [0, 0, 255]; // B, G, R

var lower_threshold = [0, 0, 0];
var upper_threshold = [80, 80, 80];

var portNumber = 5;

const client = arDrone.createClient();

var pngStream = client.getPngStream();

var s = new cv.ImageStream();



// takeoff();
s.on('error', function(err) {
  console.log("An error occurred: " + err);
  }).on('data', function(im) {
    width = im.width()
    height = im.height()

    var out = new cv.Matrix(height, width);
    var first = new cv.Matrix(height, width);

    im_canny = im.copy();
    //im_qr = im.copy();
    im_canny.save('./result/qr.png');

    im_canny.inRange(lower_threshold, upper_threshold);

    im_canny.canny(lowThresh, highThresh);

      
    im_canny.dilate(1);
    im_canny.erode(1);

    contours = im_canny.findContours();

    for (i = 0; i < contours.size(); i++){

      if (contours.area(i) < minArea){
          continue;
      }
          
      var arcLength = contours.arcLength(i, true);
      contours.approxPolyDP(i, 0.01 * arcLength, true);

      switch (contours.cornerCount(i)){
        case 4:
          out.drawContour(contours, i, RED);
          var points = [
            contours.point(i, 0),
            contours.point(i, 1),
            contours.point(i, 2),
            contours.point(i, 3)]
          break;
        case 5:
          out.drawContour(contours, i, RED);
          break;
        default:
          //out.drawContour(contours, i, WHITE);
      }
    }
  

    if(!points){
      console.log('ingen firkanter');
      return;
    } 
    
    var punkt1;
    var punkt2;
    var punkt3;
    var punkt4;

    var xGennemsnit = (points[3].x+points[2].x+points[1].x+points[0].x)/4;
    var yGennemsnit = (points[3].y+points[2].y+points[1].y+points[0].y)/4;

    for (var i = 0; i < points.length; i++) {
      if(points[i].x <= xGennemsnit){
        if(points[i].y <= yGennemsnit){
          punkt1 = [points[i].x, points[i].y];
        }
        else{
          punkt2 = [points[i].x, points[i].y];
        }
      }
      else{
        if(points[i].y <= yGennemsnit){
          punkt4 = [points[i].x, points[i].y];
        }
        else{
          punkt3 = [points[i].x, points[i].y];
        }
      }
    }

    if(!punkt1 || !punkt2 || !punkt3 || !punkt4){
      return;
    }


    var y1 = Math.sqrt((punkt1[0] - punkt2[0])*(punkt1[0]-punkt2[0])+(punkt1[1]-punkt2[1])*(punkt1[1]-punkt2[1]));
    var y2 = Math.sqrt((punkt3[0] - punkt4[0])*(punkt3[0]-punkt4[0])+(punkt3[1]-punkt4[1])*(punkt3[1]-punkt4[1]));
    
    //console.log('Grøn er ' + y1 + ' og blå er ' + y2);

    if(y1 < y2){
      if((y1/y2*100)>95) {
        console.log(y1/y2*100);
        console.log('lige');

        readQR('./result/qr.png', function(ret) {
          console.log(ret);
          if(ret == 0) {
            console.log('wallie');
          }
          else if(ret == portNumber) {
            console.log('correct port');
            console.log('Looking for circle. Setting dillerState to true.')
            circleHeight();
            console.log("Waiting on circleHeight");

            // circleHight();

            // search for circles 

            // drone.client.after()
            // stream.once('data', (mat) => {
            //   console.log('cirker');
            //   mat.convertGrayscale();
            //   mat.gaussianBlur([9, 9]);
              
            //   let circlesRead = mat.houghCircles(0.5, 200);
            //   let circles = [];
              
            //   circlesRead.forEach((c) => {
            //     console.log("Fandt en cirkel");
            //     let circle = new Circle(mat.width()/2 - c[0], mat.height()/2 - c[1]);
            //     circles.push(circle);
            //   });
              
            //   callback(circles);
            // });

          }
          else{
            //search for new qr
          }
        });
      }
      else{
        //console.log('venstre');
      }
    }
    else{
      if((y2/y1*100)>95) {
        console.log(y2/y1*100);
        console.log('lige');
        
         //return 0 for wall, 1,2,3 for ports

        readQR('./result/qr.png', function(ret) {
          console.log(ret);
          if(ret == 0) {
            console.log('wallie');
          }
          else if(ret == portNumber) {
            console.log('correct port');
            circleHeight();
            // search for circles 
          }
          else{
            //search for new qr
          }
        });
        
      }
      else{
        //console.log('højre');
      }
    }
  
});

client.getPngStream().pipe(s);


function readQR (qrfile, done) {

  var c = fs.readFileSync(qrfile);
  var p = new PNG(c);

  p.decode((data) => {

    var qr = new QrCode();
    
    qr.callback = (result, err) => {
      if(result) {
        console.log("Result is: ");
        console.log(result);

        if(result.charAt(0).localeCompare('W')==0){
          console.log('wall');
          done(0)
          return;
        }
        else {
          done(result.charAt(3));
          return;
        }

        
      }
      else {
        console.log(err);
        return 'None';
      }
    };

    qr.decode(p, data);
  });

}

function takeoff () {
  drone.client.takeoff();

  drone.client.after(5000, () => {
    drone.setYVel(0.3);
  }).after(1700, () => {
    drone.stop();
  });


  // setTimeout(function(){
  //   drone.setYVel(0.3);
  //   setTimeout(function(){
  //     console.log('stop');
  //     drone.stop();
  //   },1700);
  // },5000);
  
}

function land () {
  drone.client.land();
  console.log('land');
}

function circleHeight () {
  console.log("Going to circle height");
  drone.setYVel(0.3);

  drone.client.after(3200, () => {
    drone.stop();
  }).after(2000, () => {
    console.log("landing from inside circleHeight");
    drone.client.land();
  });
}

function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

