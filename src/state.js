class State {
    constructor() {
        if(this.constructor === State) {
            throw new Error('State must be subclassed.');
        }
    }

    update(drone) {
        throw new Error('update() must be overridden.');
    }
}

module.exports = State;