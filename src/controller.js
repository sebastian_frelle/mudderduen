const arDrone = require('ar-drone');
const fs = require('fs');
const QrCode = require('qrcode-reader');
const PNG = require('png-js');

const drone = require('./drone.js');
const dataController = require('./image_processor.js');

const client = arDrone.createClient();
let imgStream = new cv.ImageStream();
client.getPngStream().pipe(imgStream);

// Bootstrap image data workflow
imgStream.once('data', imgStreamDataHandler);

function imgStreamDataHandler(data) {
  dataController(data).then((res) => {
    // TODO handle successful response by advancing state
  }).catch((res) => {
    console.log(res);
    imgStream.once('data', imgStreamDataHandler);
  });
}
