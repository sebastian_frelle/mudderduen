const arDrone = require('ar-drone');
const keypress = require('keypress');
const cv = require('opencv');

function clamp(num, min, max) {
    if (num <= min) {
        return min;
    } else if (num >= max) {
        return max;
    } else {
        return num;
    }
}

const client = arDrone.createClient({imageSize: "1280x720"});

const drone = {
    xVel: 0,
    yVel: 0,
    zVel: 0,
    port: 0,
    image: null,
    flying: false,
    rotationVel: 0,
    altitude: 0,
    usingFrontCam: true,
    client: client,
};

drone.setXVel = function (vel) {
    this.xVel = clamp(vel, -1, 1);

    if (this.xVel > 0) {
        client.right(this.xVel);
    } else {
        client.left(Math.abs(this.xVel));
    }
};

drone.setYVel = function (vel) {
    this.yVel = clamp(vel, -1, 1);

    if (this.yVel > 0) {
        client.up(this.yVel);
    } else {
        client.down(Math.abs(this.yVel));
    }
};

drone.setZVel = function (vel) {
    this.zVel = clamp(vel, -1, 1);

    if (this.zVel > 0) {
        client.front(this.zVel);
    } else {
        client.back(Math.abs(this.zVel));
    }
};

drone.setRot = function (vel) {
    this.rotationVel = clamp(vel, -1, 1);

    if (this.rotationVel > 0) {
        client.clockwise(this.rotationVel);
    } else {
        client.counterClockwise(Math.abs(this.rotationVel));
    }
};

drone.stop = function () {
    this.xVel = 0;
    this.yVel = 0;
    this.zVel = 0;
    this.rotationVel = 0;

    client.stop();
};

drone.switchCam = function() {
    if(this.usingFrontCam) {
        this.client.config('video:video_channel', 3);
        this.usingFrontCam = false;
    } else {
        this.client.config('video:video_channel', 0);
        this.usingFrontCam = true;
    }
};

client.on('navdata', function (navdata) {
    if (!navdata.demo) {
        return;
    }

    if (navdata.demo.altitudeMeters) {
        drone.altitude = navdata.demo.altitudeMeters;
    }

    if (navdata.demo.batteryPercentage) {
        drone.battery = navdata.demo.batteryPercentage;
    }
});

keypress(process.stdin);

process.stdin.on('keypress', function (ch, key) {
    if (key) {
        if (key.name === 'space') {
            console.log("Emergency landing...");
            drone.stop();
            drone.client.land();
        } else if (key.name === 'c') {
            if (key.ctrl) {
                if (!drone.flying) {
                    console.log("Exiting process...");
                    process.exit();
                } else {
                    console.log('land before quitting');
                }
            }
        }
    }
});

process.stdin.setRawMode(true);
process.stdin.resume();

let imgStream = new cv.ImageStream();
drone.client.getPngStream().pipe(imgStream);
imgStream.on('data', (image) => {
    drone.image = image;
});

drone.client.config('CONTROL:outdoor','FALSE');
drone.client.config('CONTROL:flight_without_shell', 'TRUE');
drone.client.config('video:video_channel', 0);

module.exports = drone;