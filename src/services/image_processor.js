const Png = require('png-js');
const fs = require('fs');
const exec = require('child_process').exec;

const c = require('./../constants.js');

const qrPath = __dirname + '/../../tmp/qr.png';
const qrPhpPath = __dirname + '/../../php_qr_test/qr.php';

exports.processPhoto = (frame) => {
    frame.inRange(c.image.lower_threshold, c.image.upper_threshold);
    frame.canny(c.image.lowThresh, c.image.highThresh);
    frame.dilate(1);
    frame.erode(1);

    return frame;
};

exports.detectQuadrilateral = (frame) => {
    let contours = frame.findContours();

    for (let i = 0; i < contours.size(); i++) {
        if (contours.area(i) < c.image.minArea) {
            continue;
        }

        let arcLength = contours.arcLength(i, true);
        contours.approxPolyDP(i, 0.01 * arcLength, true);

        if (contours.cornerCount(i) !== 4) {
            return null;
        }

        return [
            contours.point(i, 0),
            contours.point(i, 1),
            contours.point(i, 2),
            contours.point(i, 3),
        ];
    }
};

exports.getLeftRightCorners = (points) => {
    let xAvg = points.reduce((res, p) => res + p.x, 0) / points.length;

    let corners = {
        leftSide: [],
        rightSide: [],
    };

    for (var i = 0; i < points.length; i++) {
        if (points[i].x <= xAvg) {
            corners.leftSide.push(points[i]);
        } else {
            corners.rightSide.push(points[i]);
        }
    }

    return corners;
}

exports.getSideLengths = (corners) => {
    let sides = {};

    sides.left = Math.sqrt(
        Math.pow(Math.abs(corners.leftSide[0].x - corners.leftSide[1].x), 2) + Math.pow(Math.abs(corners.leftSide[0].y - corners.leftSide[1].y), 2)
    );

    sides.right = Math.sqrt(
        Math.pow(Math.abs(corners.rightSide[0].x - corners.rightSide[1].x), 2) + Math.pow(Math.abs(corners.rightSide[0].y - corners.rightSide[1].y), 2)
    );

    return sides;
};

exports.readQrCode = (path, callback) => {
    exec(`php ${qrPhpPath} ${path}`, (err, stdout, stderr) => {
        if (err) {
            callback(err);
            return;
        }

        callback(null, stdout);
    });
};

const Circle = function (horizontal, vertical, radius) {
    this.horizontal = horizontal;
    this.vertical = vertical;
    this.radius = radius;
};

exports.findCircles = (frame) => {
    if(frame.empty()){
        return null;
    }
    frame.convertGrayscale();
    frame.gaussianBlur([9, 9]);
    
    let circlesRead = frame.houghCircles(0.5, 200);
    let circles = [];

    circlesRead.forEach((c) => {
        circles.push(new Circle(
            frame.width() / 2 - c[0],
            frame.height() / 2 - c[1],
            c[2]
        ));
    });
    return circles;
};