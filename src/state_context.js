module.exports = class StateContext {
    constructor(drone) {
        this.drone = drone;
        this.currentState = null;
    }

    setState(state) {
        this.currentState = state;
    }

    handle() {
        let nextState = this.currentState.update(this.drone);
        return nextState;
    }
};