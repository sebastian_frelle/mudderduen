const State = require('./../state.js');
const imageProcessor = require('./../services/image_processor.js');

const qrPath = __dirname + '/../../tmp/qr.png';

class QRLookoutState extends State {
    constructor() {
        super();
    }

    update(drone) {
        return new Promise((resolve, reject) => {
            if (!drone.image) {
                console.log('No image');

                return setTimeout(() => {
                    return resolve('qrlookout');
                }, 100);
            }

            let frame = drone.image.copy();
            frame.save(qrPath);

            let processedFrame = imageProcessor.processPhoto(frame);
            if (!processedFrame) {
                console.log('No processed frame');
                return resolve('qrlookout');
            }

            let quadrilateral = imageProcessor.detectQuadrilateral(processedFrame);
            if (!quadrilateral) {
                console.log('No quadrilateral found');
                return resolve('lookaround');
            }

            let corners = imageProcessor.getLeftRightCorners(quadrilateral);
            if (!corners) {
            } else if (corners.leftSide.length !== 2 || corners.rightSide.length !== 2) {
                console.log('No quadrilateral found with 2 left and 2 right corners');
                return resolve('lookaround');
            }

            let sides = imageProcessor.getSideLengths(corners);
            if (!sides) {
                console.log('No sides');
                return resolve('lookaround');
            } else if (!sides.left || !sides.right) {
                console.log('Could not calculate size of left and/or right side of quadrilateral');
                return resolve('lookaround');
            }

            let ratio = (sides.left / sides.right * 100);
            console.log(1, ratio);

            if (ratio < 95) {
                console.log('Going left');
                drone.setXVel(-0.05);
                return setTimeout(function () {
                    drone.stop();
                    resolve('qrlookout');
                });
            }

            ratio = (sides.right / sides.left * 100);
            console.log(2, ratio);

            if (ratio < 95) {
                console.log('Going right');
                drone.setXVel(0.05);
                return setTimeout(function () {
                    drone.stop();
                    resolve('qrlookout');
                });
            }
            console.log("Reading QR code...");

            imageProcessor.readQrCode(qrPath, (err, result) => {
                if (err) {
                    console.log(err);
                    return resolve('lookaround');
                }

                console.log("Found QR with value: " + result);
                if (result.charAt(0) === 'W') {
                    console.log('Found wall');
                    return resolve('lookaround');
                }

                let idx = parseInt(result.charAt(3));
                if (!idx) {
                    console.log('Could not parse index');
                    return resolve('lookaround');
                }

                drone.port++;
                return resolve('ringaltitude');
            });
        });
    }
}

module.exports = QRLookoutState;