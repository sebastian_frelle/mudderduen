const State = require('./../state.js');

class RingAltitudeState extends State {
    constructor() {
        super();
    }

    update(drone) {
        return new Promise((resolve, reject) => {

            // From qr altitude to ring code altitude
            console.log('Going from QR altitude to ring altitude');
            drone.setYVel(0.5);
            setTimeout(function() {
                //console.log("Ring: " + drone.altitude);
                console.log('stopper');
                drone.stop();
                return resolve('ringlookout');
            }, 1700);
                
        });
    }
}

module.exports = RingAltitudeState;