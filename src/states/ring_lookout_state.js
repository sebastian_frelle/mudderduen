const State = require('./../state.js');

const imageProcessor = require('./../services/image_processor.js');
const fs = require('fs');

const path = __dirname + '/pictureRing/';

class RingLookoutState extends State {
    constructor() {
        super();
    }

    update(drone) {
        return new Promise((resolve, reject) => {
            // look for ring
            if (!drone.image) {
                console.log("no picture");
                setTimeout(function () {
                    return resolve('ringlookout');
                }, 1000);
            } else {
                let circles = imageProcessor.findCircles(drone.image.copy());

                if (!circles || circles.length === 0) {
                    console.log("no circles");
                    drone.setZVel(-0.05);
                    setTimeout(function () {
                        drone.stop();
                    }, 500);
                } else {
                    var maxCircle = circles[0];
                    circles.forEach((c) => {
                        if (c.radius > maxCircle.radius) {
                            maxCircle = c;
                        }
                    });

                    console.log("Radius " + maxCircle.radius + " x: " + maxCircle.horizontal + " y: " + maxCircle.vertical);
                    console.log("højde: " + drone.altitude);

                    console.log("Saving image with filename " + path + Math.floor(maxCircle.horizontal));
                    drone.image.save(path + `${Date.now()}--` + Math.floor(maxCircle.horizontal) + '.png');

                    if(maxCircle.horizontal < -30){
                        console.log('højre');
                        drone.setXVel(0.1);
                        setTimeout(function () {
                            drone.stop();
                        }, 700);
                    }
                    else if(maxCircle.horizontal > 10) {
                        console.log('venstre');
                        drone.setXVel(-0.05);
                        setTimeout(function () {
                            drone.stop();
                        }, 700);
                    }

                    if (maxCircle.vertical < -50) {
                        console.log('ned');
                        drone.setYVel(-0.05);
                        setTimeout(function () {
                            drone.stop();
                        }, 1000);
                    } else if (maxCircle.vertical > 20) {
                        console.log('op');
                        drone.setYVel(0.05);
                        setTimeout(function () {
                            drone.stop();
                        }, 1000);
                    }

                    if(maxCircle.radius<100) {
                        console.log('langt frem');
                        drone.setZVel(0.2);
                        setTimeout(function () {
                            drone.stop();
                        }, 700);
                    }
                    else if(maxCircle.radius<150){
                        console.log('kort frem');
                        drone.setZVel(0.2);
                        setTimeout(function () {
                            drone.stop();
                        }, 300);
                    }

                    if(maxCircle.horizontal >= -30 && maxCircle.horizontal <= 10 && maxCircle.vertical >= -50 && maxCircle.vertical <= 20 && maxCircle.radius>=150){
                        console.log('flyv igennem');
                        drone.stop();
                        return resolve('flythroughring');
                    }
                }

                setTimeout(function () {
                    return resolve('ringlookout');
                }, 1000);
            }
        });
    }
}

module.exports = RingLookoutState;