const State = require('./../state.js');

class QRAltitudeState extends State {
    constructor() {
        super();
    }

    update(drone) {
        return new Promise((resolve, reject) => {
            if (drone.altitude < 1) {
                // From takeoff altitude to QR code altitude
                console.log('Going to QR code altitude');
                //console.log("after takeoff: " + drone.altitude);
                drone.setYVel(0.4);
                setTimeout(function() {
                    //console.log("Qr: " + drone.altitude);
                    console.log('stopper');
                    drone.stop();
                    //resolve('qrlookout');
                    return resolve('ringaltitude');
                }, 1400);
            } else {
                // From circle altitude to QR code altitude
                console.log('Going to QR code altitude from above');
                drone.setYVel(-0.5);
                setTimeout(function() {
                    console.log("højde: " + drone.altitude);
                    console.log('stopper');
                    drone.stop();
                    return resolve(null);
                }, 1700);
            }
        });
    }
}

module.exports = QRAltitudeState;