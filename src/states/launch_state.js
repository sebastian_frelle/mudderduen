const State = require('./../state.js');

class LaunchState extends State {
    constructor() {
        super();
    }

    update(drone) {
        return new Promise((resolve, reject) => {
            console.log("Take off: " + drone.altitude);
            drone.client.takeoff();
            drone.client.after(5000, function () {
                drone.stop();
                console.log('takeoff done.');
                resolve('qraltitude');
            });
        });
    }
}

module.exports = LaunchState;