const State = require('./../state.js');

class LookAroundState extends State {
    constructor() {
        super();
    }

    update(drone) {
        return new Promise((resolve, reject) => {
            console.log('----> Rotating');
            drone.setRot(0.1);
            setTimeout(function () {
                drone.stop();
                console.log('----> Stop');
                return resolve('qrlookout');
            }, 300);
        });
    }
}

module.exports = LookAroundState;