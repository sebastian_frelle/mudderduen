const State = require('./../state.js');

class FlyThroughRingState extends State {
    constructor() {
        super();
    }

    update(drone) {
        return new Promise((resolve, reject) => {
        	if (drone.port<=1){
        		// stor ring
        		console.log('stor ring gennemflyvning');
        		drone.setZVel(0.3);
        		setTimeout(function() {
        			drone.stop();
        			console.log('igennem ring');
                    return resolve ('qraltitude');
        		}, 1200);
        		
        	}
        });
    }
}

module.exports = FlyThroughRingState;