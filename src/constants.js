exports.image = {
  lowThresh: 0,
  highThresh: 100,
  minArea: 1500,
  RED: [0, 0, 255], // B, G, R
  lower_threshold: [0, 0, 0],
  upper_threshold: [80, 80, 80],
};

exports.fs = {
  qrPath: './../tmp/qr.png',
};

exports.nav = {
  portNumber: 5,
};