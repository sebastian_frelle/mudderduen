const State = require('./../state.js');
const StateContext = require('./../state_context.js');
const drone = require('./../drone.js');

class FirstState extends State {
    constructor() {
        super();
    }

    update(drone) {
        console.log('FirstState update()');
       
        return new Promise((resolve, reject) => {
            setTimeout(function() {
                resolve('secondState');
            }, 2000);
        });
    }
}

class SecondState extends State {
    constructor() {
        super();
    }

    update(drone) {
        console.log('SecondState update()');
        
        return new Promise((resolve, reject) => {
            resolve( 'thirdState');
        });
    }
}

class ThirdState extends State {
    constructor() {
        super();
    }

    update(drone) {
        console.log('ThirdState update()');

        return new Promise((resolve, reject) => {
            resolve('firstState');
        });
    }
}

const stateMapping = {};
stateMapping['firstState'] = new FirstState();
stateMapping['secondState'] = new SecondState();
stateMapping['thirdState'] = new ThirdState();

const stateContext = new StateContext(drone);

stateContext.setState(stateMapping['firstState']);
var promise;

promise = stateContext.handle()
promise.then(onHandled);

function onHandled(nextState) {
    if(nextState === null) {
        return;
    }

    let state = stateMapping[nextState];

    if(!state) {
        throw new Error('No state with id ' + nextState + ' in stateMapping.');
    }

    stateContext.setState(state);
    promise = stateContext.handle();
    promise.then(onHandled);
}
